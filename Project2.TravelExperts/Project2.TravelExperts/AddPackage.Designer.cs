﻿namespace Project2.TravelExperts
{
    partial class AddPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uxPackName = new System.Windows.Forms.TextBox();
            this.uxCommission = new System.Windows.Forms.TextBox();
            this.uxStart = new System.Windows.Forms.DateTimePicker();
            this.uxEnd = new System.Windows.Forms.DateTimePicker();
            this.uxOK = new System.Windows.Forms.Button();
            this.uxCancel = new System.Windows.Forms.Button();
            this.uxDescription = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.uxBasePrice = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Package Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 66);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Start Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 104);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "End Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 142);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Base Price";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 180);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(134, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Agency Commission";
            // 
            // uxPackName
            // 
            this.uxPackName.Location = new System.Drawing.Point(173, 28);
            this.uxPackName.Name = "uxPackName";
            this.uxPackName.Size = new System.Drawing.Size(200, 23);
            this.uxPackName.TabIndex = 1;
            // 
            // uxCommission
            // 
            this.uxCommission.Location = new System.Drawing.Point(173, 180);
            this.uxCommission.Name = "uxCommission";
            this.uxCommission.Size = new System.Drawing.Size(200, 23);
            this.uxCommission.TabIndex = 5;
            // 
            // uxStart
            // 
            this.uxStart.Location = new System.Drawing.Point(173, 66);
            this.uxStart.Name = "uxStart";
            this.uxStart.Size = new System.Drawing.Size(200, 23);
            this.uxStart.TabIndex = 2;
            // 
            // uxEnd
            // 
            this.uxEnd.Location = new System.Drawing.Point(173, 104);
            this.uxEnd.Name = "uxEnd";
            this.uxEnd.Size = new System.Drawing.Size(200, 23);
            this.uxEnd.TabIndex = 3;
            // 
            // uxOK
            // 
            this.uxOK.Location = new System.Drawing.Point(173, 264);
            this.uxOK.Name = "uxOK";
            this.uxOK.Size = new System.Drawing.Size(75, 36);
            this.uxOK.TabIndex = 7;
            this.uxOK.Text = "OK";
            this.uxOK.UseVisualStyleBackColor = true;
            this.uxOK.Click += new System.EventHandler(this.uxOK_Click);
            // 
            // uxCancel
            // 
            this.uxCancel.Location = new System.Drawing.Point(274, 264);
            this.uxCancel.Name = "uxCancel";
            this.uxCancel.Size = new System.Drawing.Size(75, 36);
            this.uxCancel.TabIndex = 8;
            this.uxCancel.Text = "Cancel";
            this.uxCancel.UseVisualStyleBackColor = true;
            this.uxCancel.Click += new System.EventHandler(this.uxCancel_Click);
            // 
            // uxDescription
            // 
            this.uxDescription.Location = new System.Drawing.Point(173, 223);
            this.uxDescription.Name = "uxDescription";
            this.uxDescription.Size = new System.Drawing.Size(200, 23);
            this.uxDescription.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Description";
            // 
            // uxBasePrice
            // 
            this.uxBasePrice.Location = new System.Drawing.Point(173, 142);
            this.uxBasePrice.Name = "uxBasePrice";
            this.uxBasePrice.Size = new System.Drawing.Size(200, 23);
            this.uxBasePrice.TabIndex = 4;
            // 
            // AddPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 330);
            this.Controls.Add(this.uxBasePrice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.uxDescription);
            this.Controls.Add(this.uxCancel);
            this.Controls.Add(this.uxOK);
            this.Controls.Add(this.uxEnd);
            this.Controls.Add(this.uxStart);
            this.Controls.Add(this.uxCommission);
            this.Controls.Add(this.uxPackName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddPackage";
            this.Text = "Add New Package";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox uxPackName;
        private System.Windows.Forms.TextBox uxCommission;
        private System.Windows.Forms.DateTimePicker uxStart;
        private System.Windows.Forms.DateTimePicker uxEnd;
        private System.Windows.Forms.Button uxOK;
        private System.Windows.Forms.Button uxCancel;
        private System.Windows.Forms.TextBox uxDescription;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox uxBasePrice;
    }
}