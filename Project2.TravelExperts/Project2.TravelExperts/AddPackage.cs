﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExpertsLibrary;

namespace Project2.TravelExperts
{
    public partial class AddPackage : Form
    {
        public AddPackage()
        {
            InitializeComponent();

            // Set the Format type and the CustomFormat string.
            uxStart.Format = DateTimePickerFormat.Custom;
            uxStart.CustomFormat = "dddd, MMMM dd, yyyy";
            uxEnd.Format = DateTimePickerFormat.Custom;
            uxEnd.CustomFormat = "dddd, MMMM dd, yyyy";
        }

        private void uxCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uxOK_Click(object sender, EventArgs e)
        {
           Package package = new Package();

            string msg = PackageManager.ValidateInput(uxPackName.Text, uxDescription.Text,
               uxBasePrice.Text, uxCommission.Text, uxStart.Value, uxEnd.Value);

           if (msg == null)
           {
               //Create new package object then add to database
               package.PkgName = uxPackName.Text;
               package.PkgStartDate = uxStart.Value;
               package.PkgEndDate = uxEnd.Value;
               package.PkgDesc = uxDescription.Text;
               package.PkgBasePrice = Convert.ToDecimal(uxBasePrice.Text);
               package.PkgAgencyCommission = Convert.ToDecimal(uxCommission.Text);
               PackageManager.AddPackage(package);
           }
           else
           {
            MessageBox.Show(msg, "Unable to submit new package", MessageBoxButtons.OK);
           }
        }
    }
}
