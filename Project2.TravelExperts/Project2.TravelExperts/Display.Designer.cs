﻿namespace Project2.TravelExperts
{
    partial class Display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxPackage = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.uxPackage)).BeginInit();
            this.SuspendLayout();
            // 
            // uxPackage
            // 
            this.uxPackage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.uxPackage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxPackage.Location = new System.Drawing.Point(0, 0);
            this.uxPackage.Name = "uxPackage";
            this.uxPackage.Size = new System.Drawing.Size(542, 217);
            this.uxPackage.TabIndex = 0;
            // 
            // Display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 217);
            this.Controls.Add(this.uxPackage);
            this.Name = "Display";
            this.Text = "List of Packages";
            ((System.ComponentModel.ISupportInitialize)(this.uxPackage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView uxPackage;
    }
}

