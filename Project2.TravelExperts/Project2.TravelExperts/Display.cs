﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExpertsLibrary;

namespace Project2.TravelExperts
{
    public partial class Display : Form
    {
        public Display()
        {
            InitializeComponent();
            //Load list of packages
            uxPackage.DataSource = PackageManager.GetPackages();

            //Format columns
            uxPackage.Columns["Products_Suppliers"].Visible = false;

            uxPackage.Columns["PkgStartDate"].HeaderText = "Start Date";
            uxPackage.Columns["PkgStartDate"].Width = 150;
            uxPackage.Columns["PkgStartDate"].DefaultCellStyle.Format = "dd'/'MM'/'yyyy";

            uxPackage.Columns["PkgEndDate"].HeaderText = "End Date";
            uxPackage.Columns["PkgEndDate"].Width = 150;
            uxPackage.Columns["PkgEndDate"].DefaultCellStyle.Format = "dd'/'MM'/'yyyy";
        }
    }
}
