﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TravelExpertsLibrary;

namespace Project2.TravelExperts
{
    public partial class UpdatePackage : Form
    {
        public UpdatePackage()
        {
            InitializeComponent();
        }

        private void UpdatePackage_Load(object sender, EventArgs e)
        {
            packageBindingSource.DataSource = PackageManager.GetPackages();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   }
}
