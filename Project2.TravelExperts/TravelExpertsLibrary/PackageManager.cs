﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelExpertsLibrary
{
    /// <summary>
    /// PackageManager class is a wrapper class that encapsulates the 
    /// repository called TravelExpertsContext
    /// </summary>
    /// <remarks>
    /// This class is an implementation of the Repository Pattern.
    /// </remarks>
        public class PackageManager
        {
            /// <summary>
            /// Private static TravelExpertsContext object serving as the repository
            /// </summary>
            private static TravelExpertsContext _repository = new TravelExpertsContext();

            /// <summary>
            /// Static method returns a travel package list collection. 
            /// </summary>
            /// <returns>Travel package collection as IList</returns>
            public static IList<Package> GetPackages()
            {
                return _repository.Packages.ToList();
            }

             /// <summary>
             /// Static method adds a package to the repository and saves to the data source.
             /// </summary>
             /// <param name="package">The package object to be added to the system</param>
             public static void AddPackage(Package package)
             {
                 _repository.Packages.Add(package);
                 _repository.SaveChanges();
             }

            public static void UpdatePackage(Package packages)
             {
                 //maybe use a bool or try & catch
                 var package = _repository.Packages.First(i => i.PackageId == packages.PackageId);
                 package.PkgAgencyCommission = 12; 
                 _repository.SaveChanges();
            }

            public static void DeletePackage(Package packages)
             {
                 //maybe use a bool or try & catch
                 var package = _repository.Packages.First(i => i.PackageId == packages.PackageId);
                 _repository.Packages.Remove(package);
                 package.PkgAgencyCommission = 12;
                 _repository.SaveChanges();
             }

            public static string ValidateInput(string name, string description,
               string basePrice, string commission, DateTime startDate, DateTime endDate)
            {
                string msg = null;
                               
                if (name == "" || description == "" || commission == "" || basePrice == "")
                {
                    //Checks package name and description are filled.
                    msg = "Please complete the fields required.\n";
                }
                else
                {
                    if (Convert.ToDecimal(commission) > Convert.ToDecimal(basePrice))
                    {
                        //Checks to see if commission is less than base price.
                        msg = "Commission must be less than the base price. \n";
                    }
                    
                    //Checks to see if End date is greater than start date.
                    if (endDate.Year == startDate.Year)
                    {
                        if (endDate.Month == startDate.Month)
                        {
                            if (endDate.Date < startDate.Date)
                            {
                                msg = "Package end date must be greater than the Package start date. \n";
                            }
                        }
                        else if (endDate.Month < startDate.Month)
                        {
                            msg = "Package end date must be greater than the Package start date. \n";
                        }                        
                    }
                    else if (endDate.Year < startDate.Year)
                    {
                      msg = "Package end date must be greater than the Package start date. \n";
                    }
                }                 
                return msg;
            }
     }
}
